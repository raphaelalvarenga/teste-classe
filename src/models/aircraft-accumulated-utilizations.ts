export class AircraftAccumulatedUtilizations {
    id?: number;
    accumulatedUntilDate: any;
    averageUtilization: any;
    compAccumulatedUtilization: number;
    dateWhen: Date;
    utilizationControl: string;

    constructor() {
        this.accumulatedUntilDate = undefined;
        this.averageUtilization = undefined;
        this.compAccumulatedUtilization = undefined;
        this.dateWhen = undefined;
        this.utilizationControl = undefined;
    }
}