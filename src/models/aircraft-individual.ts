import { AircraftAccumulatedUtilizations } from './aircraft-accumulated-utilizations';

export class AircraftIndividual {
    accumulatedUtilizations: Array<AircraftAccumulatedUtilizations> = [];
}