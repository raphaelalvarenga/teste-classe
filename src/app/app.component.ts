import { Component, OnInit } from '@angular/core';
import { AircraftAccumulatedUtilizations } from 'src/models/aircraft-accumulated-utilizations';
import { AircraftIndividual } from 'src/models/aircraft-individual';

class AircraftIndividualGrouped extends AircraftIndividual {
  // accumulatedUtilizations: Array<Array<AircraftAccumulatedUtilizations>> = [];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'teste-classe';

  utilizationControlItems = [
    'APU Cycles',
    'APU Hours',
    'Arrest',
    'Calendar',
    'Catapults',
    'Component Hours',
    'CDM Hours',
    'Date of Expiration',
    'Date of Installation',
    'Engine Cycles',
    'Engine Hours',
    'Equivalent Hour',
    'Extractions Airdrops Cycles',
    'Flight Cycles',
    'Flight Hours',
    'Hoist Cycles',
    'Landings',
    'Retirement Index Numbe',
    'Shots',
    'Thrust Reverse Cycle',
    'Winch Cycles'
  ];
  
  accumulatedUtilizations: AircraftAccumulatedUtilizations[] = [];
  accumulatedUtilizationsGrouped: Array<Array<AircraftAccumulatedUtilizations>> = [];

  constructor() {}

  ngOnInit() {
    this.addAccumulatedUtilization();
    this.addGroupedAccumulatedUtilization();
  }

  addAccumulatedUtilization() {
    this.accumulatedUtilizations.push(new AircraftAccumulatedUtilizations());
  }

  deleteAccumulatedUtilizations(index: number) {
    this.accumulatedUtilizations.splice(index, 1);
  }

  addGroupedAccumulatedUtilization() {
    this.accumulatedUtilizationsGrouped.push([{
      utilizationControl: '',
      accumulatedUntilDate: undefined,
      averageUtilization: undefined,
      compAccumulatedUtilization: undefined,
      dateWhen: undefined
    }])
  }

  addPairedAccumulatedUtilization(index: number, tipo: string) {
    this.accumulatedUtilizationsGrouped[index].push({
      utilizationControl: 'Engine Hours',
      accumulatedUntilDate: undefined,
      averageUtilization: undefined,
      compAccumulatedUtilization: undefined,
      dateWhen: undefined
    })
  }

  changeGroupedAccumulatedUtilizations(index: number) {
    if (this.accumulatedUtilizationsGrouped[index].length === 2) {
      this.accumulatedUtilizationsGrouped[index].splice(1, 1);
    }
  }

  deleteGroupedAccumulatedUtilizations(index: number) {
    this.accumulatedUtilizationsGrouped.splice(index, 1);
  }

  monitorar(indice) {
    indice === 1 ? console.log('%cEXEMPLO DA ESTRUTURA ATUAL', 'color: red') : console.log('%cEXEMPLO DA ESTRUTURA PROPOSTA', 'color: blue');
    console.log(indice === 1 ? this.accumulatedUtilizations : this.accumulatedUtilizationsGrouped);
  }
}
